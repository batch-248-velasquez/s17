console.log ("Hello World!");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
		let fullName = prompt ("Enter your full name.");
		let age = prompt ("How old are you? ");
		let currentLocation = prompt ("Where are you from? ");
		console.log ("Hello, " + fullName);
		console.log ("You are " + age);
		console.log ("You live in " + currentLocation);

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.

	
*/

	//second function here:

		function faveArtist () {
			let favArt1 = "Side A Band"
			let favArt2 = "Eraserheads"
			let favArt3 = "The Company"
			let favArt4 = "The Dawn"
			let favArt5 = "Guns N' Roses"
		
			console.log ("My favorite music artist:");
			console.log ("1. " + favArt1);
			console.log ("2. " + favArt2);
			console.log ("3. " + favArt3);
			console.log ("4. " + favArt4);
			console.log ("5. " + favArt5);
}
			faveArtist ();



/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:


			function faveMovies () {
			let favMov1 = "Shawshank Redemption"
			let favMov2 = "Men of Honor"
			let favMov3 = "The Rookie"
			let favMov4 = "Air Force One"
			let favMov5 = "Regarding Henry"
		
			console.log ("My favorite movies:");
			console.log ("1. " + favMov1);
			console.log ("Rotten Tomatoes Rating: 91%")
			console.log ("2. " + favMov2);
			console.log ("Rotten Tomatoes Rating: 42%")
			console.log ("3. " + favMov3);
			console.log ("Rotten Tomatoes Rating: 84%")
			console.log ("4. " + favMov4);
			console.log ("Rotten Tomatoes Rating: 78%")
			console.log ("5. " + favMov5);
			console.log ("Rotten Tomatoes Rating: 45%")
}
			faveMovies ();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

function printUsers() {
// let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
	};

printUsers ();



// console.log(friend1);
// console.log(friend2);